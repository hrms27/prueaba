#include <Partitions.h>


void createpartition(char *pathstring,int sizedisk,char unitchar,char *fitstring,char typechar,char *namestring){
    MBR mbr;
    int primarypartition = 0;
    int extendedpartition = 0;
    int createtypepartition = 0;
    int sizecontrol = 0;
    int initbyte = 0;
    int endbyte = 0;
    int initbyte2 = 0;
    int endbyte2 = 0;
    int expectedsize = 0;
    char one = '1';

    if(unitchar == 'k'){
        sizecontrol = 1024;
    }else if(unitchar == 'm'){
        sizecontrol = 1048576;
    }else if(unitchar == 'b'){
        sizecontrol = 1;
    }


    disk = fopen(pathstring,"rb+");

    if(disk != NULL){
        rewind(disk);

        fread(&mbr,sizeof(MBR),1,disk);
        expectedsize = sizedisk * sizecontrol;
        initbyte = sizeof(MBR) + 1;
        endbyte = mbr.sizedisk;
        initbyte2 = initbyte;
        endbyte2 = endbyte;

        int i=0;
        for(i=0;i<4;i++){
            if(mbr.partitions[i].typepartition == 'p'){;
                primarypartition += 1;
            }else if(mbr.partitions[i].typepartition == 'e'){
                extendedpartition += 1;
            }
        }

        if(typechar=='p'){
            if((primarypartition < 3 && extendedpartition == 1) || (primarypartition < 4 && extendedpartition == 0)){
                createtypepartition = 1;
            }
        }else if(typechar == 'e'){
            if((primarypartition < 4 ) && (extendedpartition == 0)){
                createtypepartition = 2;
            }
        }else if(typechar == 'l'){
            if(extendedpartition == 1){
                createtypepartition = 3;
            }
        }

        if(createtypepartition == 1 || createtypepartition == 2){
            int find = 0;
            int j = 0;
            int partitionmodify1 = 0;
            int partitionmodify2 = 0;

            for(j=0;j<4;j++){
                if(mbr.partitions[j].statuspartition == 'a'){
                    if(find == 1){
                        if((initbyte2 - mbr.partitions[j].initbytepartition) >= expectedsize){
                            endbyte2 =  mbr.partitions[j].initbytepartition;
                            partitionmodify2 = j - 1;
                        }else{
                            partitionmodify2 = j + 1;
                            initbyte2 = mbr.partitions[j].initbytepartition + mbr.partitions[j].sizepartition;
                        }
                    }else{
                        if((initbyte - mbr.partitions[j].initbytepartition) >= expectedsize){
                            find = 1;
                            partitionmodify1 = j - 1;
                            partitionmodify2 = j + 1;
                            endbyte =  mbr.partitions[j].initbytepartition;
                            initbyte2 = mbr.partitions[j].initbytepartition + mbr.partitions[j].sizepartition;
                        }else{
                            partitionmodify1 = j + 1;
                            initbyte = mbr.partitions[j].initbytepartition + mbr.partitions[j].sizepartition;
                            initbyte2 = initbyte;
                        }
                    }
                }
            }

            int k = 0;

            if(fitstring[0] == 'f'){
                if((endbyte - initbyte) >= expectedsize){
                    fseek(disk,initbyte,SEEK_SET);
                    if(typechar == 'e'){
                        EBR ebr;
                        ebr.fit = 'w';
                        ebr.status = 'n';
                        ebr.start = initbyte;
                        ebr.size = 0;
                        ebr.next = -1;
                        strcpy(ebr.name,"");
                        fwrite(&ebr, sizeof(EBR), 1, disk);
                    }
                    for(k=0;k<expectedsize;k++){
                        fwrite(&one, sizeof(char), 1, disk);
                    }
                    mbr.partitions[partitionmodify1].fitpartition = fitstring[0];
                    mbr.partitions[partitionmodify1].statuspartition = 'a';
                    mbr.partitions[partitionmodify1].initbytepartition = initbyte;
                    strcpy(mbr.partitions[partitionmodify1].namepartition,namestring);
                    mbr.partitions[partitionmodify1].sizepartition = expectedsize;
                    mbr.partitions[partitionmodify1].typepartition = typechar;
                    printf("se creo la particion correctamente \n");
                }else if((endbyte2 - initbyte2) >= expectedsize){
                    fseek(disk,initbyte2,SEEK_SET);
                    if(typechar == 'e'){
                        EBR ebr;
                        ebr.fit = 'w';
                        ebr.status = 'n';
                        ebr.start = initbyte2;
                        ebr.size = 0;
                        ebr.next = -1;
                        strcpy(ebr.name,"");
                        fwrite(&ebr, sizeof(EBR), 1, disk);
                    }
                    for(k=0;k<expectedsize;k++){
                        fwrite(&one, sizeof(char), 1, disk);
                    }
                    mbr.partitions[partitionmodify2].fitpartition = fitstring[0];
                    mbr.partitions[partitionmodify2].statuspartition = 'a';
                    mbr.partitions[partitionmodify2].initbytepartition = initbyte2;
                    strcpy(mbr.partitions[partitionmodify2].namepartition,namestring);
                    mbr.partitions[partitionmodify2].sizepartition = expectedsize;
                    mbr.partitions[partitionmodify2].typepartition = typechar;
                    printf("se creo la particion correctamente \n");
                }else{
                    printf("no se creo la particion, no hay espacio suficiente \n");
                }
            }else if(fitstring[0] == 'b'){
                if(((endbyte - initbyte) >= (endbyte2 - initbyte2)) && ((endbyte2 -initbyte2) >= expectedsize)){
                    fseek(disk,initbyte2,SEEK_SET);
                    if(typechar == 'e'){
                        EBR ebr;
                        ebr.fit = 'w';
                        ebr.status = 'n';
                        ebr.start = initbyte2;
                        ebr.size = 0;
                        ebr.next = -1;
                        strcpy(ebr.name,"");
                        fwrite(&ebr, sizeof(EBR), 1, disk);
                    }
                    for(k=0;k<expectedsize;k++){
                        fwrite(&one, sizeof(char), 1, disk);
                    }
                    mbr.partitions[partitionmodify2].fitpartition = fitstring[0];
                    mbr.partitions[partitionmodify2].statuspartition = 'a';
                    mbr.partitions[partitionmodify2].initbytepartition = initbyte2;
                    strcpy(mbr.partitions[partitionmodify2].namepartition,namestring);
                    mbr.partitions[partitionmodify2].sizepartition = expectedsize;
                    mbr.partitions[partitionmodify2].typepartition = typechar;
                    printf("se creo la particion correctamente \n");
                }else if(((endbyte2 - initbyte2) >= (endbyte - initbyte)) && ((endbyte - initbyte) >= expectedsize)){
                    fseek(disk,initbyte,SEEK_SET);
                    if(typechar == 'e'){
                        EBR ebr;
                        ebr.fit = 'w';
                        ebr.status = 'n';
                        ebr.start = initbyte;
                        ebr.size = 0;
                        ebr.next = -1;
                        strcpy(ebr.name,"");
                        fwrite(&ebr, sizeof(EBR), 1, disk);
                    }
                    for(k=0;k<expectedsize;k++){
                        fwrite(&one, sizeof(char), 1, disk);
                    }
                    mbr.partitions[partitionmodify1].fitpartition = fitstring[0];
                    mbr.partitions[partitionmodify1].statuspartition = 'a';
                    mbr.partitions[partitionmodify1].initbytepartition = initbyte;
                    strcpy(mbr.partitions[partitionmodify1].namepartition,namestring);
                    mbr.partitions[partitionmodify1].sizepartition = expectedsize;
                    mbr.partitions[partitionmodify1].typepartition = typechar;
                    printf("se creo la particion correctamente \n");
                }else{
                    printf("no se creo la particion, no hay espacio suficiente \n");
                }
            }else if(fitstring[0] == 'w'){
                if(((endbyte - initbyte) >= (endbyte2 - initbyte2)) && ((endbyte2 - initbyte2) >= expectedsize)){
                    fseek(disk,initbyte,SEEK_SET);
                    if(typechar == 'e'){
                        EBR ebr;
                        ebr.fit = 'w';
                        ebr.status = 'n';
                        ebr.start = initbyte;
                        ebr.size = 0;
                        ebr.next = -1;
                        strcpy(ebr.name,"");
                        fwrite(&ebr, sizeof(EBR), 1, disk);
                    }
                    for(k=0;k<expectedsize;k++){
                        fwrite(&one, sizeof(char), 1, disk);
                    }
                    mbr.partitions[partitionmodify1].fitpartition = fitstring[0];
                    mbr.partitions[partitionmodify1].statuspartition = 'a';
                    mbr.partitions[partitionmodify1].initbytepartition = initbyte;
                    strcpy(mbr.partitions[partitionmodify1].namepartition,namestring);
                    mbr.partitions[partitionmodify1].sizepartition = expectedsize;
                    mbr.partitions[partitionmodify1].typepartition = typechar;
                    printf("se creo la particion correctamente \n");
                }else if(((endbyte2 - initbyte2) >= (endbyte - initbyte)) && ((endbyte -initbyte) >= expectedsize)){
                    fseek(disk,initbyte2,SEEK_SET);
                    if(typechar == 'e'){
                        EBR ebr;
                        ebr.fit = 'w';
                        ebr.status = 'n';
                        ebr.start = initbyte2;
                        ebr.size = 0;
                        ebr.next = -1;
                        strcpy(ebr.name,"");
                        fwrite(&ebr, sizeof(EBR), 1, disk);
                    }
                    for(k=0;k<expectedsize;k++){
                        fwrite(&one, sizeof(char), 1, disk);
                    }
                    mbr.partitions[partitionmodify2].fitpartition = fitstring[0];
                    mbr.partitions[partitionmodify2].statuspartition = 'a';
                    mbr.partitions[partitionmodify2].initbytepartition = initbyte2;
                    strcpy(mbr.partitions[partitionmodify2].namepartition,namestring);
                    mbr.partitions[partitionmodify2].sizepartition = expectedsize;
                    mbr.partitions[partitionmodify2].typepartition = typechar;
                    printf("se creo la particion correctamente \n");
                }else{
                    printf("no se creo la particion, no hay espacio suficiente \n");
                }
            }
        }else if(createtypepartition == 3){
            int k = 0;
            int extendedpartitionsize = 0;
            int extendedpartitioninit = 0;
            EBR ebr;
            EBR ebr2;
            EBR ebr3;
            EBR initebr;
            EBR endebr;
            int find = 0;

            initebr.start = -1;
            endebr.start = -1;

            for(k=0;k<4;k++){
                if(mbr.partitions[k].typepartition == 'e'){
                    extendedpartitionsize = mbr.partitions[k].sizepartition;
                    extendedpartitioninit = mbr.partitions[k].initbytepartition;
                }
            }

            fseek(disk,extendedpartitioninit,SEEK_SET);
            fread(&ebr,sizeof(EBR),1,disk);

            if(ebr.status == 'n' && ebr.next == -1){
                if(extendedpartitionsize >= expectedsize){
                    printf("entro \n");
                    fseek(disk,extendedpartitioninit,SEEK_SET);
                    ebr.fit = fitstring[0];
                    strcpy(ebr.name,namestring);
                    ebr.next = -1;
                    ebr.size = expectedsize;
                    ebr.start = extendedpartitioninit;
                    ebr.status = 'a';
                    fwrite(&ebr,sizeof(EBR),1,disk);
                    int m = 0;
                    for(m=0;m<(expectedsize - sizeof(EBR));m++){
                        fwrite(&one,sizeof(char),1,disk);
                    }
                    printf("particion logica creada\n");
                }else{
                    printf("El tamano de la particion extendida no es suficiente \n");
                }
            }else{
                do{
                    if(fitstring[0] == 'f'){
                        if(ebr.next == -1){
                            if(((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size)) >= expectedsize){
                                fseek(disk,ebr.start + ebr.size,SEEK_SET);
                                ebr2.fit = fitstring[0];
                                strcpy(ebr2.name,namestring);
                                ebr2.next = -1;
                                ebr2.size = expectedsize;
                                ebr2.start = ebr.start + ebr.size;
                                ebr2.status = 'a';
                                fwrite(&ebr2,sizeof(EBR),1,disk);
                                int m = 0;
                                for(m=0;m<(expectedsize - sizeof(EBR));m++){
                                    fwrite(&one,sizeof(char),1,disk);
                                }
                                fseek(disk,ebr.start,SEEK_SET);
                                ebr.next = ebr2.start;
                                fwrite(&ebr,sizeof(EBR),1,disk);
                                find = 1;
                            }else{
                                find = -1;
                                printf("El tamano de la particion extendida no es suficiente \n");
                            }
                        }else{
                            ebr2 = ebr;
                            fseek(disk,ebr2.next,SEEK_SET);
                            fread(&ebr,sizeof(EBR),1,disk);
                            if((ebr.start - (ebr2.start + ebr2.size)) >= expectedsize){
                                fseek(disk,ebr2.start + ebr2.size,SEEK_SET);
                                ebr3.fit = fitstring[0];
                                strcpy(ebr3.name,namestring);
                                ebr3.next = ebr.start;
                                ebr3.size = expectedsize;
                                ebr3.start = ebr2.start + ebr2.size;
                                ebr3.status = 'a';
                                fwrite(&ebr3,sizeof(EBR),1,disk);
                                int m = 0;
                                for(m=0;m<(expectedsize - sizeof(EBR));m++){
                                    fwrite(&one,sizeof(char),1,disk);
                                }
                                fseek(disk,ebr2.start,SEEK_SET);
                                ebr2.next = ebr3.start;
                                fwrite(&ebr2,sizeof(EBR),1,disk);
                                find = 1;
                            }
                        }
                    }else if(fitstring[0] == 'b'){
                        if(ebr.next == -1){
                            if(!((initebr.start == -1) && (endebr.start == -1))){
                                if(((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size)) >= expectedsize){
                                    initebr = ebr;
                                    endebr.start = -1;
                                }
                            }else{
                                if((endebr.start - (initebr.start + initebr.size)) >= ((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size))){
                                    if(((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size)) >= expectedsize){
                                        initebr = ebr;
                                        endebr.start = -1;
                                    }
                                }
                            }

                            if(!(initebr.start == -1)){
                                fseek(disk,initebr.start + initebr.size,SEEK_SET);
                                ebr2.fit = fitstring[0];
                                strcpy(ebr2.name,namestring);
                                if(endebr.start == -1){
                                    ebr2.next = -1;
                                }else{
                                    ebr2.next = endebr.start;
                                }
                                ebr2.size = expectedsize;
                                ebr2.start = initebr.start + initebr.size;
                                ebr2.status = 'a';
                                fwrite(&ebr2,sizeof(EBR),1,disk);
                                int m = 0;
                                for(m=0;m<(expectedsize - sizeof(EBR));m++){
                                    fwrite(&one,sizeof(char),1,disk);
                                }
                                fseek(disk,initebr.start,SEEK_SET);
                                initebr.next = ebr2.start;
                                fwrite(&initebr,sizeof(EBR),1,disk);
                                find = 1;
                                printf("se creo la particion \n");
                            }else{
                                find = -1;
                                printf("No hay espacio suficiente \n");
                            }
                        }else{
                            ebr2 = ebr;
                            fseek(disk,ebr2.next,SEEK_SET);
                            fread(&ebr,sizeof(EBR),1,disk);
                            if(initebr.start == -1 && endebr.start == -1){
                                if((ebr.start - (ebr2.start + ebr2.size)) >= expectedsize){
                                    initebr = ebr2;
                                    endebr = ebr;
                                }
                            }else{
                                if((endebr.start - (initebr.start + initebr.size)) >= (ebr.start - (ebr2.start + ebr2.size))){
                                    if((ebr.start - (ebr2.start + ebr2.size)) >= expectedsize){
                                        initebr = ebr2;
                                        endebr = ebr;
                                    }
                                }
                            }
                        }
                    }else if(fitstring[0] == 'w'){
                        if(ebr.next == -1){
                            if((initebr.start == -1) && (endebr.start == -1)){
                                if(((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size)) > expectedsize){
                                    initebr = ebr;
                                    endebr.start = -1;
                                }
                            }else{
                                if((endebr.start - (initebr.start + initebr.size)) < ((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size))){
                                    if(((extendedpartitionsize + extendedpartitioninit) - (ebr.start + ebr.size)) > expectedsize){
                                        initebr = ebr;
                                        endebr.start = -1;
                                    }
                                }
                            }

                            if(!(initebr.start == -1)){
                                fseek(disk,initebr.start + initebr.size,SEEK_SET);
                                ebr2.fit = fitstring[0];
                                strcpy(ebr2.name,namestring);
                                if(endebr.start == -1){
                                    ebr2.next = -1;
                                }else{
                                    ebr2.next = endebr.start;
                                }
                                ebr2.size = expectedsize;
                                ebr2.start = initebr.start + initebr.size;
                                ebr2.status = 'a';
                                fwrite(&ebr2,sizeof(EBR),1,disk);
                                int m = 0;
                                for(m=0;m<(expectedsize - sizeof(EBR));m++){
                                    fwrite(&one,sizeof(char),1,disk);
                                }
                                fseek(disk,initebr.start,SEEK_SET);
                                initebr.next = ebr2.start;
                                fwrite(&initebr,sizeof(EBR),1,disk);
                                find = 1;
                                printf("Se creo la particion logica \n");
                            }else{
                                find = -1;
                                printf("No hay espacio suficiente \n");
                            }
                        }else{
                            ebr2 = ebr;
                            fseek(disk,ebr2.next,SEEK_SET);
                            fread(&ebr,sizeof(EBR),1,disk);
                            if(initebr.start == -1 && endebr.start == -1){
                                if((ebr.start - (ebr2.start + ebr2.size)) > expectedsize){
                                    initebr = ebr2;
                                    endebr = ebr;
                                }
                            }else{
                                if((endebr.start - (initebr.start + initebr.size)) < (ebr.start - (ebr2.start + ebr2.size))){
                                    if((ebr.start - (ebr2.start + ebr2.size)) >= expectedsize){
                                        initebr = ebr2;
                                        endebr = ebr;
                                    }
                                }
                            }
                        }
                    }
                }while(find == 0);
            }
        }else{
            printf("no se puede crear la particion \n");
        }

        rewind(disk);
        fwrite(&mbr,sizeof(MBR),1,disk);

        fclose(disk);
    }else{
        printf("El disco que desea abrir no existe \n");
    }

}

void deletepartition(char *pathstring,int deleteformp,char *namestring){

    MBR mbr;
    int extendedpartition = 0;
    int extendedpartitionstart = 0;
    int find = 0;
    char cero = '0';
    disk = fopen(pathstring,"rb+");

    if(disk != NULL){
        rewind(disk);
        fread(&mbr,sizeof(MBR),1,disk);

        int k=0;
        for(k=0;k<4;k++){
            if(strcmp(mbr.partitions[k].namepartition,namestring)==0){
                if(find ==0){
                    find =1;
                    mbr.partitions[k].statuspartition = 'n';
                    mbr.partitions[k].typepartition = '0';
                    mbr.partitions[k].fitpartition = 'w';
                    mbr.partitions[k].sizepartition = 0;
                    mbr.partitions[k].initbytepartition = 0;
                    strcpy(mbr.partitions[k].namepartition,"");
                    rewind(disk);
                    fwrite(&mbr,sizeof(MBR),1,disk);
                    if(deleteformp == 5){
                        fseek(disk,mbr.partitions[k].initbytepartition,SEEK_SET);
                        int l = 0;
                        for(l = 0;l<mbr.partitions[k].sizepartition;l++){
                            fwrite(&cero,sizeof(char),1,disk);
                        }
                    }
                    printf("Particion eliminada %s \n",namestring);
                }
            }
            if(mbr.partitions[k].typepartition == 'e'){
                extendedpartition = 1;
                extendedpartitionstart = mbr.partitions[k].initbytepartition;
            }
        }

        if((find == 0) && (extendedpartition == 1)){
            EBR ebr;
            EBR ebr2;
            ebr2.start = -1;
            int initpart = 0;

            initpart = extendedpartitionstart;

            do{
                fseek(disk,initpart,SEEK_SET);
                fread(&ebr,sizeof(EBR),1,disk);

                if(strcmp(ebr.name,namestring)==0){
                    if(find == 0){
                        find = 1;
                        ebr.status = 'n';
                        strcpy(ebr.name,"");
                        ebr.fit = 'w';
                        fseek(disk,initpart,SEEK_SET);

                        if(deleteformp == 4){
                            fwrite(&ebr,sizeof(EBR),1,disk);
                        }else if(deleteformp == 5){
                            int l = 0;
                            for(l = 0;l<ebr.size;l++){
                                fwrite(&cero,sizeof(char),1,disk);
                            }
                        }

                        if(ebr2.start > -1){
                            ebr2.next = ebr.next;
                            fseek(disk,ebr2.start,SEEK_SET);
                            fwrite(&ebr2,sizeof(EBR),1,disk);
                        }

                        printf("Particion eliminada %s %d \n",namestring,ebr.next);

                    }
                }
                initpart = ebr.next;
                ebr2 = ebr;
            }while(ebr.next > -1 && find == 0);


        }

        if(find == 0){
            printf("No se encontro la particion indicada %s\n",namestring);
        }

        fclose(disk);
    }else{
        printf("No existe el disco \n");
    }

}


void addpartition(int addsize,char *pathstring, char unitchar, char *namestring){

    MBR mbr;
    int extendedpartition = 0;
    int extendedpartitionstart = 0;
    int extendedpartitionsize = 0;
    int find = 0;
    int sizecontrol = 0;
    int newsize = 0;

    if(unitchar == 'k'){
        sizecontrol = 1024;
    }else if(unitchar == 'm'){
        sizecontrol = 1048576;
    }else if(unitchar == 'b'){
        sizecontrol = 1;
    }

    newsize = addsize * sizecontrol;
    disk = fopen(pathstring,"rb+");

    if(disk != NULL){
        rewind(disk);
        fread(&mbr,sizeof(MBR),1,disk);

        int k=0;
        for(k=0;k<4;k++){
            if(strcmp(mbr.partitions[k].namepartition,namestring)==0){
                if(find ==0){
                    find =1;
                    if(k<3){
                        if((mbr.partitions[k + 1].initbytepartition - mbr.partitions[k].initbytepartition - mbr.partitions[k].sizepartition) >= newsize){
                            mbr.partitions[k].sizepartition = mbr.partitions[k].sizepartition + newsize;
                            rewind(disk);
                            fwrite(&mbr,sizeof(MBR),1,disk);
                            printf("Se modifico la particion %s\n",namestring);
                        }
                    }else{
                        if((mbr.sizedisk - mbr.partitions[k].initbytepartition - mbr.partitions[k].sizepartition) >= newsize){
                            mbr.partitions[k].sizepartition = mbr.partitions[k].sizepartition + newsize;
                            rewind(disk);
                            fwrite(&mbr,sizeof(MBR),1,disk);
                            printf("Se modifico la particion %s\n",namestring);
                        }
                    }
                }
            }
            if(mbr.partitions[k].typepartition == 'e'){
                extendedpartition = 1;
                extendedpartitionstart = mbr.partitions[k].initbytepartition;
                extendedpartitionsize = mbr.partitions[k].sizepartition;
            }
        }

        if(find == 0 && extendedpartition == 1){
            EBR ebr;
            int initpart = 0;

            initpart = extendedpartitionstart;

            do{
                fseek(disk,initpart,SEEK_SET);
                fread(&ebr,sizeof(EBR),1,disk);

                if(strcmp(ebr.name,namestring)==0){
                    if(find == 0){
                        find = 1;
                        if(ebr.next == -1){
                            if(((extendedpartitionstart + extendedpartitionsize) - ebr.start - ebr.size ) >= newsize){
                                ebr.size = ebr.size + newsize;
                                fseek(disk,ebr.start,SEEK_SET);
                                fwrite(&ebr,sizeof(EBR),1,disk);
                                printf("Se modifico la particion %s\n",namestring);
                            }
                        }else{
                            if((ebr.next - ebr.start - ebr.size ) >= newsize){
                                ebr.size = ebr.size + newsize;
                                fseek(disk,ebr.start,SEEK_SET);
                                fwrite(&ebr,sizeof(EBR),1,disk);
                                printf("Se modifico la particion %s\n",namestring);
                            }
                        }
                    }
                }
                initpart = ebr.next;
            }while(ebr.next > -1 && find == 0);

        }else{
            printf("No se encontro la particion indicada\n");
        }
    fclose(disk);
    }else{
        printf("No existe el disco \n");
    }

}
